package models;
import javax.persistence.*;

import java.io.Serializable;
@Entity
@Table(name="musica")
public class Musica implements Serializable{
    @Id
    @Column(name="idMusica")
    private int idMusica;
    @Column(name="idGrupo")
    private int idGrupo;
    @Column(name="titulo")
    private String titulo;
    @Column(name="soporte")
    private String soporte;
    @Column(name="genero")
    private String genero;

    public Musica(){

    }

    public Musica(int idMusica, int idGrupo, String titulo, String soporte, String genero) {
        this.idMusica = idMusica;
        this.idGrupo = idGrupo;
        this.titulo = titulo;
        this.soporte = soporte;
        this.genero = genero;
    }

    public Musica(String idMusica, String idGrupo, String titulo, String soporte, String genero) {
    }

    @Override
    public String toString() {
        return "Musica{" +
                "idMusica=" + idMusica +
                ", idGrupo=" + idGrupo +
                ", titulo='" + titulo + '\'' +
                ", soporte='" + soporte + '\'' +
                ", genero='" + genero + '\'' +
                '}';
    }

    public int getIdMusica() {
        return idMusica;
    }

    public void setIdMusica(int idMusica) {
        this.idMusica = idMusica;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSoporte() {
        return soporte;
    }

    public void setSoporte(String soporte) {
        this.soporte = soporte;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
