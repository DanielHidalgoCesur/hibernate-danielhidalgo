package models;
import javax.persistence.*;

import java.io.Serializable;
@Entity
@Table(name="grupo")
public class Grupo implements Serializable{
    @Id
    @Column(name="idGrupos")
    private int idGrupos;
    @Column(name="nombre")
    private String nombre;
    @Column(name="cancion")
    private String cancion;
    @Column(name="origen")
    private String origen;
    @Column(name="vocalista")
    private String vocalista;

    public Grupo(){

    }

    public Grupo(int idGrupos, String nombre, String cancion, String origen, String vocalista) {
        this.idGrupos = idGrupos;
        this.nombre = nombre;
        this.cancion = cancion;
        this.origen = origen;
        this.vocalista = vocalista;
    }

    public Grupo(String idGrupos, String nombre, String cancion, String origen, String vocalista) {
    }

    public int getIdGrupos() {
        return idGrupos;
    }

    public void setIdGrupos(int idGrupo) {
        this.idGrupos = idGrupo;
    }

    @Override
    public String toString() {
        return "Grupo{" +
                "idGrupo=" + idGrupos +
                ", nombre='" + nombre + '\'' +
                ", cancion='" + cancion + '\'' +
                ", origen='" + origen + '\'' +
                ", vocalista='" + vocalista + '\'' +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCancion() {
        return cancion;
    }

    public void setCancion(String cancion) {
        this.cancion = cancion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getVocalista() {
        return vocalista;
    }

    public void setVocalista(String vocalista) {
        this.vocalista = vocalista;
    }
}
