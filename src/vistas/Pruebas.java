package vistas;


import org.hibernate.Session;

import models.Discografia;
import models.Grupo;
import models.Musica;
import utilidades.Utilidades;





public class Pruebas {

	public static void main(String[] args) {
		Discografia discografia = new Discografia();
		
		System.out.println(discografia);
		
		discografia.setEstante(132);
		discografia.setGrupo("El mono amelio");
		discografia.setTituloDisco("Los chumachus");
		discografia.setSoporte("mesa");
		discografia.setIdMusica(1234);
		
		System.out.println(discografia);
		


		Session session = Utilidades.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(discografia);

		session.getTransaction().commit();
		
		Grupo grupo = new Grupo();
		System.out.println(grupo);
		
		grupo.setCancion("El del medio de los chichoooos");
		grupo.setNombre("Estopa");
		grupo.setOrigen("Españita");
		grupo.setIdGrupos(13);
		grupo.setVocalista("Estopita");
		System.out.println(grupo);

		session.beginTransaction();
		session.save(grupo);
		session.getTransaction().commit();


		
		Musica musica = new Musica();
		System.out.println(musica);
		
		musica.setGenero("no binario");
		musica.setIdGrupo(1234567);
		musica.setSoporte("soportable");
		musica.setIdMusica(7654321);
		musica.setTitulo("ni idea");
		System.out.println(musica);
		

		session.beginTransaction();
		session.save(musica);

		session.getTransaction().commit();
		
		
		Utilidades.getSessionFactory().close();

	}

}
